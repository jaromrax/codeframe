#!/usr/bin/env python3
"""
 Library for functions used in
 - fn_stat
 - fn_save
 - fn_cut
 - fn_selchan
"""
from fire import Fire
import os
from console import fg,bg
import pandas as pd


def read_from_file(filename, fourcolumns = False, sort = True):
    """
    TEST FUNCTION TO MIMIC OBJECT's FROM_FILE
    fill pandas dataframe - for spectrum and for asc
    self.pd_data    defined
    """
    base = os.path.splitext(filename)[0]
    name = f"d_{base}"
    src_filename = filename
    src_basename = os.path.basename(filename)
    src_path = os.path.dirname(filename)
    print(f"i... opening file /{fg.yellow}{filename}{fg.default}/ and naming /{fg.green}{name}{fg.default}/")
    df_data = pd.read_csv(filename, comment = "#", header=None,sep = r"\s+" )
    print(f"i... ... ... ... ... ... ... data loaded. LEN={len(df_data):,}")
    return df_data




def main():
    print()

if __name__=="__main__":
    Fire(main)
