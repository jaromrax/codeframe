#!/usr/bin/env python3
"""
 These modules should contain one function each...
 - I have a special function to load DF here - that mimics the original FUNC in objects
 - And Thus This module allows to be run STANDALONE
"""
import math
import tempfile
import datetime as dt
import numpy as np
import configparser
import os
import pandas as pd
from fire import Fire
from console import fg,bg

import fnlib_local
STANDALONE = False
try:
    from codeframe import config
    from codeframe import objects
except:
    STANDALONE = True
    print(f"i... {fg.yellow}STANDALONE MODE ... no objects, no config{fg.default}")
if __name__ == "__main__": STANDALONE = True

# ================================================ ASC AND INI ==================
# taken from histo_io  refers to ascgeneric....
# I like it here, bc menu interaction
#


# ----------------------------------------------ASC AND INI COPY TO TMP
def get_ini_copy(url):
    """
    INI file on HTTP:// ....
    """
    print(f"D... fetching {url}")
    filename = tempfile.NamedTemporaryFile(suffix='.ini', delete=False)
    txt = ""
    try:
        with urllib.request.urlopen( url ) as uf:
            txt = uf.read().decode('utf-8')
    except urllib.error.URLError as e:
        print("X... URL ERROR: ",e.reason)
    if len(txt)>10:
        with open( filename.name, 'w', encoding='utf8', newline='') as f:
            f.write( txt )
    return filename.name




### def read_from_file(filename, fourcolumns = False, sort = True):



def time_cut(df, t_from, t_to):
    t0 = float(t_from)
    t1 = float(t_to)
    df1 = df.loc[ (df.time>t0)&(df.time<t1)]
    return df1



def is_float(n): # FROM FLASHCAM
    try:
        float_n = float(n)
    except ValueError:
        return False
    else:
        return True


def open_parser( inifile, channel ):
    xconfigx = configparser.ConfigParser( delimiters=(" ","\t") )
    xconfigx.read( inifile)

    print(f"{fg.dimgray}D... ... ini: SECTIONS: {xconfigx.sections()} {fg.default}" )
    if len(xconfigx.sections())==0:
        print(f"X... {fg.red}NO SECTIONS FOUND IN {fg.default}",inifile)
        return None

    if channel is not None:
        if not str(channel) in xconfigx:
            print(f"X... channel {channel} {bg.red}     NOT PRESENT  - in config file    {bg.default}")
            return None
        else:
            print(f"D... {fg.green} channel {channel} IS PRESENT - in config file {fg.default}")
    return xconfigx


def get_setting( xconfigx, parameter, channel=0, retfloat = True):
    """
    search parameter like TRG_HOLDOFF in configparser - first in [CHAN] then in COMMON,  normally return float
    """
    ch = f"{channel}"
    res = None
    if  not ch in xconfigx:
        print(f"X... {fg.red}channel {channel} NOT in config {fg.default}")
        return None
    if parameter in xconfigx[ch]:
        res = xconfigx[ ch ][parameter]
        print(f"{fg.dimgray}D...  ... ini: {parameter:20s} @ch {channel}   ... {res} {fg.default}" )
    else:
        res = xconfigx[f'COMMON'][parameter]
        print(f"{fg.dimgray}D...  ... ini: {parameter:20s} @common ... {res} {fg.default}")
    if retfloat:
        if is_float(res) :
            return float(res)
        else:
            print("X... {fg.red}NOT A FLOAT NUMBER{fg.default}", res)
            return None
    return None

def extract_holdoffs( inifile, channel):
    """
    originally dpp2jpg , working in [us]
    returns TRG_HOLDOFF, PKHOLD, TWIN_US, ECALa, ECALb
      ==> limits for:  blind, double zeroes, single zeroes
    """
    xconfigx = open_parser( inifile, channel)
    if xconfigx is None: return 0,0,0
    TRG_HOLDOFF      = get_setting( xconfigx, 'TRG_HOLDOFF'      ,channel  ) # completely blind acq
    TF_PEAK_HOLDOFF  = get_setting( xconfigx, 'TF_PEAK_HOLDOFF'  ,channel  ) #
    TF_SHAPING_TIME  = get_setting( xconfigx, 'TF_SHAPING_TIME'  ,channel  ) # analog integration
    TF_SEL_PEAKMEAN  = get_setting( xconfigx, 'TF_SEL_PEAKMEAN'  ,channel  ) # AVG factor 0 1 2 ...
    TF_PEAKING_DELAY = get_setting( xconfigx, 'TF_PEAKING_DELAY' ,channel  ) #
    TF_FLAT_TOP      = get_setting( xconfigx, 'TF_FLAT_TOP'      ,channel  ) # avoid overshoot, insert pause
    ECALa            = get_setting( xconfigx, 'CALIBRATION_A'    ,channel  )
    ECALb            = get_setting( xconfigx, 'CALIBRATION_B'    ,channel  )


    WID=0.01
    if TF_SEL_PEAKMEAN==0:WID=0.01
    elif TF_SEL_PEAKMEAN==1:WID=0.01*4
    elif TF_SEL_PEAKMEAN==2:WID=0.01*16
    elif TF_SEL_PEAKMEAN==3:WID=0.01*64
    else: WID=3

    # i dont knwow why 0.16 is here --- But is seems empirically prefectly OK
    TWIN_US = round(0.16 + TF_SHAPING_TIME+TF_PEAKING_DELAY + WID, 1)
    PKHOLD  = round(0.16+  TF_SHAPING_TIME+TF_FLAT_TOP +TF_PEAK_HOLDOFF ,1)
    # =================  NEW INTERPRETATIONS.  it works - gets 10us and cleans ilogic zero; close nonzero
    TRG_LIM = TRG_HOLDOFF
    DZERO_LIM = round(  TF_SHAPING_TIME + TF_FLAT_TOP + TF_PEAK_HOLDOFF ,1)
    SZERO_LIM = round(  TF_SHAPING_TIME + TF_PEAKING_DELAY + WID, 1)

    TAG = f"{bg.green}{fg.white}"
    if TWIN_US==0 or TRG_HOLDOFF==0 or PKHOLD==0:
        f"{bg.red}{fg.white}"
    #print(f"{TAG}TRG_HOLDOFF     {TRG_HOLDOFF:5.1f} us{bg.default}{fg.default}  ... blind region bellow this time" )
    #print(f"{TAG}PKHOLD          {PKHOLD:5.1f} us{bg.default}{fg.default}  ... full data from this time" )
    #print(f"{TAG}TWIN_US         {TWIN_US:5.1f} us{bg.default}{fg.default}  ... double zeroes bellow this time" )
    print(f"{TAG}TRG_LIM     {TRG_LIM:5.1f} us{bg.default}{fg.default}  ... blind region bellow this time" )
    print(f"{TAG}DZERO_LIM     {DZERO_LIM:5.1f} us{bg.default}{fg.default}  ... full data from this time" )
    print(f"{TAG}SZERO_LIM     {SZERO_LIM:5.1f} us{bg.default}{fg.default}  ... double zeroes bellow this time" )
    #return TRG_HOLDOFF, PKHOLD, TWIN_US, ECALa, ECALb
    return TRG_LIM, DZERO_LIM, SZERO_LIM, ECALa, ECALb



def load_ini_file(filename, channel = None ): # already complete filename
    """
    origin ascgeneric -
    """
    inifilename = os.path.splitext(filename)[0]+".ini"
    if inifilename.find("http://")==0: inifilename = get_ini_copy( inifilename )
    if os.path.exists(inifilename):
        #print(f"D... {fg.green} OK - ini file exists {fg.default}")
        if channel is not None:
            #print(f"D... {fg.green} OK channel {channel} {fg.default}")
            #TRG_HOLDOFF,TF_PEAK_HOLDOFF, TWIN_US, ECALa, ECALb = extract_holdoffs( inifilename, channel )
            TRG_LIM,DZERO_LIM,SZERO_LIM, ECALa, ECALb = extract_holdoffs( inifilename, channel )
            #print(  TRG_HOLDOFF,TF_PEAK_HOLDOFF, TWIN_US)
            #return TRG_HOLDOFF,TF_PEAK_HOLDOFF, TWIN_US, ECALa, ECALb
            return TRG_LIM,DZERO_LIM,SZERO_LIM, ECALa, ECALb
        else:
            print(f"D... {fg.red} NO channel given ... {fg.default}")
        return None, None, None, 1,0 # last is calibration
    return None,None,None, 1,0 # last is calibatoin


def filename_decode_starttime(filename):
    """
    from ascgeneric
    The ONLY procedure to get the start time from filename
    """
    # should work both with 2021mmdd and  21mmddd
    # returns the start time
    basename = os.path.basename(filename)
    basename = os.path.splitext(basename)[0]
    # start-date
    startd = basename.split("_")[1]
    # start-time
    startt = basename.split("_")[2]
    # 4 digits always
    if len(startd)==6:
        #print("D...  compensating 2 digit year to 4 digit year")
        startd="20"+startd
    #print("D...  time start MARK=",startd+startt)
    start = dt.datetime.strptime(startd+startt,"%Y%m%d%H%M%S" )
    print(f"D...  {fg.dimgray}... start time obtained from filename  : {start.strftime('%a')}   {start} {fg.default}")
    return start

def filename_decode_starttag(filename):
    """
    x check   first line     #2022/10/02_14:03:49.284170 ...start
    """
    # should work both with 2021mmdd and  21mmddd
    # returns the start time

    with open(filename, 'r') as f:
        firstline = f.readline()
    if firstline[0]=="#" and firstline.find("...start")>0:
        s = firstline[1:]
        s = s.split(" ")[0]
        #print(s)
        start = dt.datetime.strptime(s, "%Y/%m/%d_%H:%M:%S.%f" )
        #print(res)
        #print("D...  ... start time obtained from TAG inside:",  start.strftime("%a"),  start)
        print(f"D...  {fg.dimgray}... start time obtained from TAG inside: {start.strftime('%a')}   {start} {fg.default}")
        return start
    else:
        print(f"X... {fg.red}) NO START TAG inside {filename} !! {fg.default}")
        return None

    basename = os.path.basename(filename)
    basename = os.path.splitext(basename)[0]
    # start-date
    startd = basename.split("_")[1]
    # start-time
    startt = basename.split("_")[2]
    # 4 digits always
    if len(startd)==6:
        #print("D...  compensating 2 digit year to 4 digit year")
        startd="20"+startd
    #print("D...  time start MARK=",startd+startt)
    start = dt.datetime.strptime(startd+startt,"%Y%m%d%H%M%S" )
    print("D...  ... start time obtained from filename ",  start.strftime("%a"),  start)
    return start



#
def erlang_cum(x,rate,k=1):
    # in the test - i supply:   1/mu == rate
    res = 0
    for n in range(k):
        res+=1/math.factorial(int(n)) * np.exp(-rate*x)*np.power(x*rate,n)
        #print(f"                 n={n},  {res}")
        #res=math.exp(-rate*x)
    return 1 - res

# ******************************************************************************************************
#

#----------------------------------------------------------------------
def asc_stat_print( df, TIMEWIN_US = None , filename = None, TRGHOLD = None, DZEROES_US = None):
#pd_detect_zeroes(df, channel,  TIMEWIN_US = 4.2 ):
    """
    RETURNS DEADTIME CAEN
    get number of zeroes, single zeroes, double zeroes, standalone zeroes
    check the WINDOW is right (E-E)==0,  check the erlang estimation
    """
    print( f"D... detection of zeroes: WINDOW == {bg.pink}{fg.black} {TIMEWIN_US:.1f} us {bg.default}{fg.default}")

    # vycisteny kanal, ----------------------------------- DFZ
    # I PREPARE SEVERAL VIEWS : only this channel :
    #dfz = df[:-1] # WHY????????????  Last event can be partial retrive but there is some fileter earlier
    chan_available = df['ch'].unique()
    if len(chan_available)>1:
        print(f"X... {fg.red} select ONE channel only - this is statistics over all channels{fg.default}")
        #stat_asc(dfz)
        return None
    channel = chan_available[0]
    #index_names = dfz[  (dfz.ch!=channel)].index
    #dfz = dfz.drop(  index_names )
    zeroes = len(  df.loc[ (df.E==0) ] )
    #print( f" ... TOTAL ZEROES  = {zeroes}")

    # same as dfz for the moment -------------------------- DFALONE IS ALONE !!!!!!!!!!
    dfalone = df # drop the close (cluster) events to count standalones
    index_names = dfalone[ (dfalone.ch!=channel)].index
    dfalone = dfalone.drop( index_names )
    # STANDALONE EVENTS  - drop all events close to another event
    index_names = dfalone[ (dfalone.dtus<=TIMEWIN_US) | (dfalone.dtuspr<=TIMEWIN_US)  ].index
    dfalone = dfalone.drop( index_names) # NOT inplace, else it changes the df!!! # LAST REDEFINITION ALONE

    #print(f"i... purely isolated events {len(dfalone)}" )
    isoevents = len( dfalone )
    isozeroes = len( dfalone.loc[ (dfalone.E==0)  ])
    isononzeroes = len( dfalone.loc[ (dfalone.E!=0)  ])
    # print(f" ... purely isolated zeroes {stazeroes}")


    # playing on clusters---------------------------- DFZ IS CLUSTERS !!!!!!!!!!!
    # ----------- COMPLEMENT TO STANDALONES ... drop all isolated, keep clusters
    dfcluster = df[~df.isin(dfalone) ].dropna()
    # COMBINED ZEROES ... PAIRS treatment --- INSIDE dfcluster --- i look forward and check that nextE is not out of cluster
    # ilogic zeroes
    izeroes = len( dfcluster.loc[ ((dfcluster.E==0) & (dfcluster.next_E!=0))  & (df.dtus<=TIMEWIN_US)  ] )
    #print( f" ... ilogiczeroes = {izeroes}" )
    szeroes = len(dfcluster.loc[ ((dfcluster.E!=0) & (dfcluster.next_E==0)) & (df.dtus<=TIMEWIN_US)  ] )
    #print( f" ... singlezeroes = {szeroes} " )
    dzeroes = len(dfcluster.loc[ ((dfcluster.E==0) & (dfcluster.next_E==0))  & (df.dtus<=TIMEWIN_US) ] )
    #print( f" ... doblezeroes = {dzeroes} " )
    dnonzeroes = len(dfcluster.loc[ ((dfcluster.E!=0) & (dfcluster.next_E!=0))  & (df.dtus<=TIMEWIN_US) ] )

    nclusters =  len(dfcluster.loc[  (df.dtus>TIMEWIN_US) ] )


    #dfz.reset_index(inplace=True)
    # print(f" ... clustered events    = {len(dfz)}")

    #index_names = dfz[ (dfz.E!=0) ].index # drop nonzeroes
    #dfz = dfz.drop( index_names )
    #print(f" ... clustered where E=0 = {len(dfz)}")

    t1 = df['time'].max()
    t0 = df['time'].min()
    rate = len(df)/(t1-t0) * 1.2
    # ---------------
    #   ERLANG ESTIMATIONS  -------------
    # ---------------------
    wind_erlang = erlang_cum(float(TIMEWIN_US)*1e-6, rate, 1)*100  # in %    bellow window 4.48us
    trgh_erlang = erlang_cum(float(TRGHOLD)*1e-6, rate, 1)*100  # in %    bellow window 4.48us
    dzer_erlang = erlang_cum(float(DZEROES_US)*1e-6, rate, 1)*100  # in %    bellow window 4.48us
    DTCAEN = zeroes/len(df)*100
    CORRESP_US = 0
    i_erlang=0
    for i in range(10,300):
        i_erlang = erlang_cum(float(i/10)*1e-6, rate, 1)*100
        CORRESP_US = round(i/10,1)
        if i_erlang>DTCAEN: break

    satun_n_ch = None # n saturations
    Tsatu_ch = None # time max in satu
    Tsatu_pr = None # percent max in satu
    if 'extras' in df:
        #print(f"D... extras    present: {df1['extras'].unique()}... 1..satur,3..roll,4..reset,8..fake" )
        satu_n_ch = len(df.loc[ (df['extras']&1)!=0] )
        Tsatu_time = df.loc[ (df['extras']&1)!=0]["prev_satu"].sum()/1e+6   # in sec...
        Tsatu_pr = 100*Tsatu_time/(t1-t0)
        #print(f"D... SATURATIONS {satu_n_ch} total: {Tsatu_ch} sec. at maximum")
        #print(f"D... SATURATIONS {satu_n_ch} total: {100*Tsatu_ch/interval:.2f} % at maximum")

        #print(f"D... number of saturations: ",len( df.loc[ (df['extras']&1)==1] ) , f" {Tsatu_ch:.2f} sec. at max;  {Tsatu_pr:.2f} % at max"   )


    output = f"""________________________________________________________________________
 {filename:40s}        {t0:,.2f} ... {t1:,.2f} sec.
________________________________________________________________________
    D... total    events  /ch{channel}/ : {len(df):8,d}
    D... isolated events  /ch{channel}/ : {isoevents:8,d}
    D... total    nonzero /ch{channel}/ : {len(df)-zeroes:8,d}
    D... isolated nonzero /ch{channel}/ : {isoevents-isozeroes:8,d}   ...   {(isoevents-isozeroes)/len(df)*100:5.2f}%
    D... total    zeroes  /ch{channel}/ : {zeroes:8,d} (0/00)~  {fg.yellow}{DTCAEN:5.2f}%  {fg.green}/DT-caen/{fg.default}
    D... isolated zeroes  /ch{channel}/ : {isozeroes:8,d} (-0-) ~  {fg.yellow}{isozeroes/len(df)*100:5.2f}% {fg.default} [glitches]
    D... single   zeroes  /ch{channel}/ : {szeroes:8,d} (E-0)    {100*szeroes/len(df):5.2f}%
    D... double   zeroes  /ch{channel}/ : {dzeroes:8,d} (0-0)    {2*100*dzeroes/len(df):5.2f}%
    D... n_sat,tmax_sat   /ch{channel}/ : {satu_n_ch:8d} (|__|)   {fg.yellow}{Tsatu_pr:5.2f}% {fg.default}  {Tsatu_time:.2f} sec. {fg.green}/DT SAT/{fg.default}
    D... ilogic   zeroes  /ch{channel}/ : {bg.pink}{fg.black}{izeroes:8,d} (0-E) {bg.default}{fg.default} /should be 0 when window is correct/
    D... close NonZeroes  /ch{channel}/ : {bg.pink}{fg.black}{dnonzeroes:8,d} (E-E) {bg.default}{fg.default}  /should be 0  -"- /
    D... blind erlang     /ch{channel}/ : {wind_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {TIMEWIN_US:.1f} us
    D... blind erlang     /ch{channel}/ : {trgh_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {TRGHOLD:.1f} us
    D... blind erlang     /ch{channel}/ : {dzer_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {DZEROES_US:.1f} us
    D... blind erlang     /ch{channel}/ : {i_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {CORRESP_US:.1f} us
_______________________________________________________________________________
"""

    """
    D... total    events (chan={channel}) = {len(df):8d}
    D... isolated events (chan={channel}) = {isoevents:8d}
    D... clusterd events (chan={channel}) = {len(df)-isoevents:8d}  (in {nclusters} clusters)

    D... total    nonzero(chan={channel}) = {len(df)-zeroes:8d}
    D... isolated nonzero(chan={channel}) = {isoevents-isozeroes:8d}
    D... clusterd nonzero(chan={channel}) = {len(df)-zeroes-isononzeroes:8d}

    D... total    zeroes (chan={channel}) = {zeroes:8d} ~ {zeroes/len(df)*100:5.2f}% {fg.green}/DT-caen/{fg.default}
    D... isolated zeroes (chan={channel}) = {isozeroes:8d} ~ {isozeroes/len(df)*100:5.2f}%  [glitches]
    D... clusterd zeroes (chan={channel}) = {zeroes-isozeroes:8d}
    D... single   zeroes (chan={channel}) = {szeroes:8d} (E-0)  /{100*szeroes/len(df):.2f} %/
    D... double   zeroes (chan={channel}) = {dzeroes:8d} (0-0)  /{2*100*dzeroes/len(df):.2f} %/ {fg.yellow}/?clusters/{fg.default}
    D... ilogic   zeroes (chan={channel}) = {izeroes:8d} (0-E)  /should be 0/
    D... double   E      (chan={channel}) = {dnonzeroes:8d} (E-E)  /should be 0/

    D... blind erlang    (chan={channel}) = {wind_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {TIMEWIN_US:.1f} us
    D... blind erlang    (chan={channel}) = {trgh_erlang:.2f} %  {fg.green}/DT from ERLANG/{fg.default} {TRGHOLD:.1f} us

    D... n_sat,tmax_sat  (chan={channel}) = {satu_n_ch:8d}, {Tsatu_ch:.2f} s. {Tsatu_pr:.1f} %  {fg.green}/DT SAT/{fg.default}
_______________________________________________________________________________
"""
####all blind10==lost ~~ all zeroes+blind0.5==lost; maybe some 0 are exagerated @ortec; NEED [satur-median] estimation HERE
    # {fg.red}D... double zeroes (chan={channel}) = {dzeroes:8d} ~  {2*dzeroes/len(df)*100:5.2f}% ( % counted correctly 2x){fg.default}
    # D... => zrs@HigherClusters  = {zeroes-isozeroes-2*dzeroes:8d}  (zeroes in longer than 2 clusters)
    # D... ______________________ end of zero detection
    # D...  window {TIMEWIN_US} us   ....  crutial for correct double zeroes detection

    print(output)
    with open("01logging.log","a") as f:
        f.write("****************************************************************************\n")
        f.write(output)
        f.write("****************************************************************************\n")

    #     # returns length for now
    #     # endblock()
    # *******************************RETURNS DEADTIME CAEN ********************************
    return zeroes/len(df)*100
    return None # zeroes,dzeroes,szeroes,izeroes,isozeroes,output



def statistics( df ):
    """
    return TSatu-max....  very basic statistics .... no need to expaded
    """
    df1 = df
    #print("i... enhancing DataFrame by dtus and dtus_prev ", end="...", flush=True)
    #df1 = enhance_by_dtus_and_next_E(df1)
    tmin = df1['time'].min()
    tmax = df1['time'].max()
    interval = tmax - tmin
    chan_available = df1['ch'].unique()

    chan = chan_available[0]
    print("_"*50,"BASICS BEGIN  ")
    print(f"    D... channels available    {fg.yellow}{chan_available}   {fg.green}  {interval:,.2f} sec. {fg.default}")
    for ich in chan_available:
        dfview = df1.loc[ df1['ch']==ich]
        chlen = len(dfview)
        cht1= dfview['time'].max()
        cht0= dfview['time'].min()
        chrate = chlen/(cht1-cht0)
        print(f"     {ich}... ... chan {fg.green}{ich}{fg.default} ...        {fg.green} {chlen:8,d} {fg.default}events;  Rate ... {fg.green}{chrate:8,.2f} {fg.default} cps")

        pileups_ch = len(dfview.loc[ dfview['pu']==1])  # (3)for 4 columns, it is fixed in table_read
        #print(f"    D... pu        present: {df1['pu'].unique()}... 1 or 0 (3v0 before 2020)" )
        print(f"     {ich}... pu        present: {dfview['pu'].unique()}...  {pileups_ch} pu==1 evts")
        #print(f"D... pu        present: {df1['pu'].unique()}... 1 or 0 (3v0 before 2020)" )
        #print(f"    D... pu       (pu==1) : {pileups_ch} ")
        if 'extras' in df:
            print(f"     {ich}... extras    present: {dfview['extras'].unique()}... 1..satur,3..roll,4..reset,8..fake" )
        #satu_n_ch = len(df1.loc[ (df1['extras']&1)!=0] )
        #Tsatu_ch = df1.loc[ (df1['extras']&1)!=0]["prev_satu"].sum()/1e+6   # in sec...
        #print(f"D... SATURATIONS {satu_n_ch} total: {Tsatu_ch} sec. at maximum")
        #print(f"D... SATURATIONS {satu_n_ch} total: {100*Tsatu_ch/interval:.2f} % at maximum")
        #
        # I HAVE THIS LATER ON
        #print(f"    D... number of saturations: {len( df1.loc[ (df1['extras']&1)==1] ):,} #=> {Tsatu_ch:,.2f} sec. at max;  {100*Tsatu_ch/interval:.2f} % at max"   )

#    print(f"    {fg.dimgray}D... Emin Emax       : {df1['E'].min()} ...  {df1['E'].max()}    /2^15={2**15}/{fg.default}")
#    print(f"    D... Tmin Tmax       : {tmin:.2f} ... {tmax:,.2f} ; total time {interval:,.2f} sec")
    print("_"*50,"BASICS END   ")


    #print(df1)
    ###print("  range of neighbours in [us]:",dtusmin, dtusmax)

    # if 'extras' in  df1:
    #     satu_n_ch = len(df1.loc[ (df1['extras']&1)!=0] )
    #     Tsatu_ch = df1.loc[ (df1['extras']&1)!=0]["prev_satu"].sum()/1e+6   # in sec...
    #     # print(f"D... SATURATIONS {satu_n_ch} total: {Tsatu_ch} sec. at maximum")
    #     #print(f"D... SATURATIONS {satu_n_ch} total: {100*Tsatu_ch/interval:.2f} % at maximum")
    # else:
    #     satu_n_ch = None
    #     Tsatu_ch = None

    #len_zeroes,len_dzeroes,len_szeroes,len_izeroes,len_stazeroes,zoutput = ascgeneric.pd_detect_zeroes(df1, chan, TIMEWIN_US=4.48) # df1[ (df1.E==0) ]
    #print(zoutput)

    #return Tsatu_ch # RETURN TSatu-max


#------------------------------------------------------------------------

def enhance_by_dtus_and_next_E(df):
    #print(f"D... broadening table with dt :next event  in t+dtus")
    df['dtus'] = (df.time.shift(-1)-df.time)*1000*1000
    df['dtus'] = df['dtus'].astype('float32') #
    dtusmin = df['dtus'].min()
    dtusmax = df['dtus'].max()
    df.fillna(99999, inplace =True)
    #print(f"D... range of time-differences (erlang) values min/max : {dtusmin:.3f} us ...  {dtusmax:.3f}  usec." )


    #print(f"D... broadening table with dt :prev event  = t - dtus (to enable search for standalones)")
    df['dtuspr'] = (df.time - df.time.shift(+1))*1000*1000
    df['dtuspr'] = df['dtuspr'].astype('float32') #
    dtusminpr = df['dtuspr'].min()
    dtusmaxpr = df['dtuspr'].max()
    df.fillna(99999, inplace =True)
    #print(f"D... range of time differences (erlang) values : {dtusminpr:.3f} us ...  {dtusmaxpr:.3f}  usec. PREVIOUS" )

    #print(f"D... broadening table - next_E")
    df['next_E'] = (df.E.shift(-1))
    df.fillna(0, inplace=True)
    df['next_E'] = df['next_E'].astype('int32') #

    #print(f"D... broadening table - next_ch")
    df['next_ch'] = (df.ch.shift(-1))
    df.fillna(0, inplace=True)
    df['next_ch'] = df['next_ch'].astype('int32') #

    if 'extras' in df.columns:
        #print(f"D... broadening table - prev_dtus SATU  previous time")
        df['prev_satu'] = (df.time - df.time.shift(+1))*1000*1000
        #  this says - if no saturation flag=> set 0
        df['prev_satu'] = np.where((df['extras']&1)==1, df['prev_satu'] , 0)
        #  same as
        # df.loc[  (df.extras&1)!=1, 'prev_satu' ] =  0
        df.fillna(0, inplace = True)
        df['prev_satu'] = df['prev_satu'].astype('float32') #
    #----------nice debug print ---------------------
    #print(df)
    #endblock()
    return df


def fill_details(ttime,ltime,dtimep, cala, calb, entries, rate, started, lastev, name):
    res = f"""totaltime {ttime:.2f}
livetime {ltime:.2f}
deadtimeprc {dtimep:.2f}
a {cala}
b {calb}
entries {entries:.0f}
rate {rate:.2f}
started {started}
lastevent {lastev}
name {name}
"""
    return res


def s_load_asc( filename, channel  , existing_df = None, fourcolumns = False, sort = True,
                ini_time = None, fini_time = None,
                save = None
               ):  # complete filena or address
    """
    loads asc file, needs ini file too
    returns DF and inifile
    """
    # f = folder
    # if len(f)==0: f = "./"
    # if f[-1]!="/": f=f+"/"

    #avadisc = list_asc_disc( folder = folder)
    # # present_avadisc(avadisc)

    inifilename = os.path.splitext(filename)[0]+".ini"
    ascfilename = os.path.splitext(filename)[0]+".asc"

    if filename.find("http://")==0:
        inifilename = get_ini_copy( inifilename )
        print(" ... local INI: ",inifilename)
    else:
        if not os.path.exists(ascfilename):
            print(f"X... {fg.red}no file {fg.default}", ascfilename)
            return existing_df, None


    if True:
        #####print(f"i... {bg.green} INI FILE PRESENT{bg.default}", inifilename)
        start_fname = filename_decode_starttime( filename )   #  - get start from filename TO REPORT
        start_exact = filename_decode_starttag( filename )
        discr =  abs(  (start_exact-start_fname).total_seconds()   )
        tag = f"{fg.green}"
        if discr>1.0:
            tag = f"{fg.red}"
        print(  "i...",tag, f" ... difference between FNAME start and TAG start is {discr} s.  {fg.default}")
        ####print("D... started", start, type(start) )
        #print(f"i...{bg.white}{fg.black} ... LOADING {ascfilename} ...                                                             {fg.default}{bg.default}")
        #wastart = dt.datetime.now()

        if existing_df is None:
            return
            #df = ascgeneric.pd_read_table( ascfilename, sort = True,  fourcolumns = False)
        df = existing_df
        if "time" not in existing_df.columns:
            #
            #  If already done, do not modify.....
            #  This is a final touch to make the DF nice
            #
            if len(df.columns)==5:
                df.columns = ['time','E','pu','ch','extras']
                df = df.dropna(axis='columns', how='all')
                df['time'] = df['time']/1e+8 # this is correct for 10ns step ADC
                # print("D... table last timestamp ",df.iloc[-1]["time"], "sec.")
                #print(df.dtypes)
                df['time'] = df['time'].astype('float64')
                df['E']    = df['E'].astype('int32')
                df['pu']   = df['pu'].astype('int32')
                df['ch']   = df['ch'].astype('int32')
                if 'extras' in df:
                    df['extras'] = df['extras'].astype('int32')
                #print("D...    finale types ************************************")
                #print(df.dtypes)

            if fourcolumns:
                print("D... extra operation for four columns ...  FOURCOLUMNS TABLE !!!")
                print(df)
                df['E'] = df['E']+ (df['pu']&1) * 16384
                df['pu'] = df['pu'].apply(lambda x: x >> 1)
                print(df)

            if sort:
                print(f"{fg.dimgray}D... sorting (after read_table):{fg.default}")
                #print(df)
                df = df.sort_values(by="time")
                #print(df)
                df.reset_index(inplace=True, drop=True)

            else:
                print("X... {fg.red} ... not 5 columns - check the earlier versions to repair {fg.default}")
                return

        #
        # I HAVE ALL BASIC THINGS....  now :  CUTS in chanel !!!BUT STATISTICS!!!  ; time;  expand
        #
        # ***************************************************************************************
        chan_available = df['ch'].unique()
        #print(chan_available)
        if channel is None:
            print(f"X... {fg.red} give me channel number (e.g.  -c 1 ) {fg.default}")
            return df, start_exact
        channel = int(channel)

        if not channel in chan_available:
            print(f"X... channel {channel} {bg.red}     NOT PRESENT - in data    {bg.default}")
            return df, start_exact
        else:
            print(f"D... {fg.green} channel {channel} IS PRESENT - in data {fg.default}")

        #
        # SUCCESS
        #
        statistics(df)
        # SELECTION OF THE CHANNEL ********************
        df =  df.loc[ (df.ch==channel) ]
        print(f"i... {fg.limegreen} only channel #{channel} data remain! {fg.default}")
        if True:
            return df, start_exact # , inifilename, start_exact
        #
        #
        #
        # **********************************************************************************************
        # operations
        # **********************************************************************************************
        if ini_time is not None and fini_time is not None:
            print(f"{bg.yellow}{fg.black}=============== OPERATION=========================={bg.default}{fg.default}")
            if ini_time>df['time'].max(): return None
            if fini_time<df['time'].min(): return None
            df = time_cut(df, ini_time, fini_time)

        # **********************************************************************************************
        # operations
        # **********************************************************************************************
        TRGHOLD_us, PKHOLD_us, DBLZERO_us, ECALa, ECALb = load_ini_file( inifilename, channel ) # I never need channel here.. Later, after CUT

        if TRGHOLD_us is None  or  PKHOLD_us is None:
            print(f"x... {bg.red}NO INI FILE PRESENT{bg.default}")
            return existing_df, start_exact
        #print(df.head(1))
        #print(df.tail(1))
        #print(  df.iloc[[0,1, -2,-1]]  )
        # **********************************************************************************************

        #wastop = dt.datetime.now()-wastart
        #print(f"i... {bg.white}{fg.green} ... LOADED {len(df)/1000/1000:.2f} Mrows ... {len(df)/wastop.total_seconds()/1000/1000:.2f} Mrows/sec ", end="")
        #print( " "*40,f"{fg.default}{bg.default}")
        print()
        print(f"{fg.dimgray}D... enhancing DataFrame by dtus and dtus_prev {fg.default}", flush=True)
        df = enhance_by_dtus_and_next_E(df)
        # ********* STATISTICS ***********
        #stat_asc(df)
        # ***********STAT ON DEADTIME ***********
        DTP = asc_stat_print(df, TIMEWIN_US=PKHOLD_us,  filename = ascfilename, TRGHOLD = TRGHOLD_us , DZEROES_US = DBLZERO_us )

        if save:
            # **********************************************************************************************
            # operations
            # **********************************************************************************************
            print(f"{bg.yellow}{fg.black}=============== OPERATION=========================={bg.default}{fg.default}")

            # **********************************************************************************************
            # operations
            # **********************************************************************************************
            origname = os.path.splitext(filename)[0]
            tag = dt.datetime.now().strftime("%y%m%d_%H%M%S")
            if ini_time is not None and fini_time is not None:
                savename = f"{origname}_ch{channel}_{tag}_{int(ini_time)}_{int(fini_time)}.txt"
                detlname = f"{origname}_ch{channel}_{tag}_{int(ini_time)}_{int(fini_time)}.details"
            else:
                savename = f"{origname}_ch{channel}_{tag}.txt"
                detlname = f"{origname}_ch{channel}_{tag}.details"

            print(f"i...  {fg.orange} ...saving ... {savename} ... {fg.default}")
            dfcol = df['E']
            himax = 4096
            binmax = himax
            narr = dfcol.to_numpy()
            his,bins = np.histogram(narr,bins=binmax,range=(0.0,himax) )
            del narr
            np.savetxt(savename, his, fmt="%d")

            # details = fill_details(ttime,ltime,dtimep, cala, calb, entries, rate, started, name)
            ttime = df['time'].max() - df['time'].min()
            ltime = ttime - DTP/100*ttime
            cala = ECALa
            calb = ECALb
            entries = len(df)
            rate = len(df)/ttime
            started = start_exact + dt.timedelta(seconds= df['time'].min() )
            lastev = start_exact + dt.timedelta(seconds= df['time'].max() )
            name = os.path.splitext(filename)[0]
            details = fill_details(ttime, ltime,DTP, cala, calb, entries, rate, started, lastev, name)
            print(details)
            with open(detlname,"w") as f:
                f.write(details)

    return df, start_exact # , inifilename, start_exact




# =====================================================================================================
# =====================================================================================================
# --------------------------    MAIN ------------------------------------------------------------------
# =====================================================================================================
# =====================================================================================================
#KNOWN_COMMANDS_LOCAL_TYPE = "FS"
KNOWN_COMMANDS_LOCAL_TYPE = "OBJ"

def require_parameter(parname, args, kwargs):
    if parname in kwargs:
        name = kwargs[parname]
        print(f"D... parname: {parname} .... type: {type(name)} ... {name}")
        return name
    elif parname in args:
        return True
    else:
        return None

# =====================================================================================================
# =====================================================================================================

def main(*args,channel=None):
    """
    select channel AND MAINLY PROCESS ASC FILE AS RUN EVENTBYENEVT  - on the dataframe object
    """
    print(f"{fg.dimgray}D... main() @fn_selchan: args/kwargs ### /{args}//{fg.default}")

    oname = ""
    if len(args)==0:
        print("D...  argument needed - type: OBJECT " )
        return None
    oname = args[0]


    # **************************************************************
    if STANDALONE:
        #df = require_parameter("existing_df", kwargs )
        channel = require_parameter("channel", args)
        if channel is None:
            print("X... {fg.red} give the channel number ( --channel 1)")
        it = require_parameter("ini_time", args )
        ft = require_parameter("fini_time", args )
        if it is None or ft is None:
            print("D... possible options for CUT:  --ini_time 60 --fini_time 120)")
        save = require_parameter("save", args )
        if save is None:
            print("D... possible options for save:  --save ")
        #
        # Using import from another local library
        df = fnlib_local.read_from_file(oname)
        s_load_asc( oname, channel, df, ini_time = it, fini_time = ft, save = save)
        #  I AM DONE - NOW I work with an object ----
        return
    # **************************************************************
    # print( oname,"?", list( objects.get_objects_list() ) )

    if objects.object_exists(oname):
        obj = objects.get_object( oname )        #print(f"i... exists  {fg.green}{oname}{fg.default} == {obj} ")
        if obj is None:
            print(f"i... {fg.red} cant get  {oname} OBJECT{fg.default}")
            return False
        if type(obj)==objects.O_dataframe:
            print(f"i... showing {fg.green}{oname}{fg.default}  ")        #objects.list_objects(  )
            #channel = require_parameter("channel", args )
            # if channel is None:
            #     print(f"X... {fg.red} give me channel number (e.g.  -c 1 ) {fg.default}")
            #     return
            #
            obj.df_data, started = s_load_asc( obj.src_filename, channel, obj.df_data)
            obj.elements = len(obj.df_data)
            obj.started = started
            #print(started)
            #print( "D... type==",type(obj) ," .. ", type(obj)==objects.O_dataframe )
            #print( obj.__dict__ )
            #obj.print()
            #print(obj.get_name() )
            #print(obj.about() )
            #print(obj.abouts() )
            # ------------------------  object contains  printme function: -----------
            # if "printme" in dir(obj):    #print("D...    printme is there...")
            #     print(obj.printme() )
            # else:
            #     return False
            #print(f"i... obj function called  {fg.green}{oname}{fg.default}  ")
        else:
            print("X... not type O_dataframe")
    #**************************************************************************
    else:
        print(f"i... {fg.red} NOT showing {oname}{fg.default}")
    return True



if __name__=="__main__":
    #Fire(read_from_file)
    Fire(main)
    # run0142_221002_232850_1mo37p2.asc
