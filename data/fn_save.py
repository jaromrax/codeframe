#!/usr/bin/env python3
"""
 These modules should contain one function each...
 - I have a special function to load DF here - that mimics the original FUNC in objects
 - And Thus This module allows to be run STANDALONE
"""
import math
import tempfile
import datetime as dt
import numpy as np
import configparser
import os
import pandas as pd
from fire import Fire
from console import fg,bg

import fnlib_local
STANDALONE = False
try:
    from codeframe import config
    from codeframe import objects
except:
    STANDALONE = True
    print(f"i... {fg.yellow}STANDALONE MODE ... no objects, no config{fg.default}")
if __name__ == "__main__": STANDALONE = True

#KNOWN_COMMANDS_LOCAL_TYPE = "FS"
KNOWN_COMMANDS_LOCAL_TYPE = "OBJ"
# ================================================ ASC AND INI ==================
# taken from histo_io  refers to ascgeneric....
# I like it here, bc menu interaction
#


def save_spect(df, filename,  DTP, ECALa, ECALb, start_exact, himax = 4096 , ini_time = None, fini_time = None):
    """
    save txt spectrum from df
    """
    # **********************************************************************************************
    # operations
    # **********************************************************************************************
    print(f"{bg.yellow}{fg.black}=============== OPERATION=========================={bg.default}{fg.default}")

    # **********************************************************************************************
    # operations
    # **********************************************************************************************
    origname = os.path.splitext(filename)[0]
    tag = dt.datetime.now().strftime("%y%m%d_%H%M%S")
    channel = df["ch"][0]
    if ini_time is not None and fini_time is not None:
        savename = f"{origname}_ch{channel}_{tag}_{int(ini_time)}_{int(fini_time)}.txt"
        detlname = f"{origname}_ch{channel}_{tag}_{int(ini_time)}_{int(fini_time)}.details"
    else:
        savename = f"{origname}_ch{channel}_{tag}.txt"
        detlname = f"{origname}_ch{channel}_{tag}.details"

    print(f"i...  {fg.orange} ...saving ... {savename} ... {fg.default}")
    dfcol = df['E']
    ### himax = 4096
    binmax = himax
    narr = dfcol.to_numpy()
    his,bins = np.histogram(narr,bins=binmax,range=(0.0,himax) )
    del narr
    print(f"D... saving {savename}")
    np.savetxt(savename, his, fmt="%d")

    # details = fill_details(ttime,ltime,dtimep, cala, calb, entries, rate, started, name)
    ttime = df['time'].max() - df['time'].min()
    ltime = ttime - DTP/100*ttime
    cala = ECALa
    calb = ECALb
    entries = len(df)
    rate = len(df)/ttime
    started = start_exact + dt.timedelta(seconds= df['time'].min() )
    lastev = start_exact + dt.timedelta(seconds= df['time'].max() )
    name = os.path.splitext(filename)[0]
    details = fnlib_local.fill_details(ttime, ltime,DTP, cala, calb, entries, rate, started, lastev, name)
    print(f"D... saving {detlname}")
    print(details)
    with open(detlname,"w") as f:
        f.write(details)

    #return df, start_exact # , inifilename, start_exact





# =====================================================================================================
# =====================================================================================================

def main(*args):
    """
    make prev - next  ... erlang transformation
    """
    print(f"{fg.dimgray}D... main() @fn_selchan: args ### /{args}/ {fg.default}")

    oname = ""
    if len(args)==0:
        print(f"D...  {fg.red}argument needed - type: OBJECT {fg.default}" )
        oname = "run0132_221002_140349_1Mo31p4.asc"
        #return None
    else:
        oname = args[0]


    if objects.object_exists(oname):
        obj = objects.get_object( oname )        #print(f"i... exists  {fg.green}{oname}{fg.default} == {obj} ")
        if obj is None:
            print(f"i... {fg.red} cant get  {oname} OBJECT{fg.default}")
            return False
        if type(obj)==objects.O_dataframe:
            print(f"i... showing {fg.green}{oname}{fg.default}  ")        #objects.list_objects(  )
            # ------------------------------------------------------------------------
            if "ch" not in obj.df_data or  "E" not in obj.df_data:
                print(f"X... {fg.red} DF columns not labeled, RUN selchan {fg.default}")
                return False

            print(obj.started,  type(obj.started) )
            if obj.started is None:
                print(f"X... {fg.red} object started is not known {fg.default}")
                return False

            inifilename = os.path.splitext(obj.src_filename)[0]+".ini"
            channel = obj.df_data['ch'][0] # representative channel
            start_exact = obj.started
            TRGHOLD_us, PKHOLD_us, DBLZERO_us, ECALa, ECALb = fnlib_local.load_ini_file( inifilename , channel )
            zeroes = len(  obj.df_data.loc[ (obj.df_data.E==0) ] )
            total = len( obj.df_data )
            DTP = zeroes/total*100
            save_spect( obj.df_data ,  obj.src_filename,
                        DTP = DTP, ECALa = ECALa, ECALb =ECALb,
                        start_exact = start_exact)
            # ------------------------------------------------------------------------
        else:
            print("X... not type O_dataframe")
    else:
        print(f"i... {fg.red} NOT showing {oname}{fg.default}")
    return True



if __name__=="__main__":
    #Fire(read_from_file)
    Fire(main)
    # run0142_221002_232850_1mo37p2.asc
