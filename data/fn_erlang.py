#!/usr/bin/env python3
"""
 These modules should contain one function each...
 - I have a special function to load DF here - that mimics the original FUNC in objects
 - And Thus This module allows to be run STANDALONE
"""
import math
import tempfile
import datetime as dt
import numpy as np
import configparser
import os
import pandas as pd
from fire import Fire
from console import fg,bg

import fnlib_local
STANDALONE = False
try:
    from codeframe import config
    from codeframe import objects
except:
    STANDALONE = True
    print(f"i... {fg.yellow}STANDALONE MODE ... no objects, no config{fg.default}")
if __name__ == "__main__": STANDALONE = True

#KNOWN_COMMANDS_LOCAL_TYPE = "FS"
KNOWN_COMMANDS_LOCAL_TYPE = "OBJ"
# ================================================ ASC AND INI ==================
# taken from histo_io  refers to ascgeneric....
# I like it here, bc menu interaction
#





# =====================================================================================================
# =====================================================================================================

def main(*args):
    """
    make prev - next  ... erlang transformation
    """
    print(f"{fg.dimgray}D... main() @fn_selchan: args ### /{args}/ {fg.default}")

    oname = ""
    if len(args)==0:
        print(f"D...  {fg.red}argument needed - type: OBJECT {fg.default}" )
        oname = "run0132_221002_140349_1Mo31p4.asc"
        #return None
    else:
        oname = args[0]


    if objects.object_exists(oname):
        obj = objects.get_object( oname )        #print(f"i... exists  {fg.green}{oname}{fg.default} == {obj} ")
        if obj is None:
            print(f"i... {fg.red} cant get  {oname} OBJECT{fg.default}")
            return False
        if type(obj)==objects.O_dataframe:
            print(f"i... showing {fg.green}{oname}{fg.default}  ")        #objects.list_objects(  )
            # ------------------------------------------------------------------------
            if "time" not in obj.df_data.columns:
                print(f"X... {fg.red} labels (columns) not defined in the DF {fg.default}")
                print(f"X... ... {fg.red} select channel : selchan {oname} -c i {fg.default}")
                return False
            DLEFT = len(obj.df_data)
            if  DLEFT==0:
                print(f"X... {fg.red} NO DATA in dataframe  {fg.default}")
                return False

            inifilename = os.path.splitext(obj.src_filename)[0]+".ini"
            channel = obj.df_data['ch'][0] # representative channel
            TRGHOLD_us, PKHOLD_us, DBLZERO_us, ECALa, ECALb = fnlib_local.load_ini_file( inifilename , channel )

            if TRGHOLD_us is None  or  PKHOLD_us is None:
                print(f"x... {bg.red}NO INI FILE PRESENT{bg.default}")
                return False

            print(f"{fg.dimgray}D... enhancing DataFrame by dtus and dtus_prev {fg.default}", flush=True)
            if "dtus" not in obj.df_data.columns:
                obj.df_data = fnlib_local.enhance_by_dtus_and_next_E(obj.df_data)
                obj.comment = f"{obj.comment} Columns with prev/next dt[us] added."

            DTP = fnlib_local.asc_stat_print(obj.df_data, TIMEWIN_US=PKHOLD_us,  filename = obj.src_filename, TRGHOLD = TRGHOLD_us , DZEROES_US = DBLZERO_us )


            # ------------------------------------------------------------------------
        else:
            print("X... not type O_dataframe")
    else:
        print(f"i... {fg.red} NOT showing {oname}{fg.default}")
    return True


if __name__=="__main__":
    #Fire(read_from_file)
    Fire(main)
    # run0142_221002_232850_1mo37p2.asc
