#!/usr/bin/env python3
"""
 These modules should contain one function each...
 - I have a special function to load DF here - that mimics the original FUNC in objects
 - And Thus This module allows to be run STANDALONE
"""
import math
import tempfile
import datetime as dt
import numpy as np
import configparser
import os
import pandas as pd
from fire import Fire
from console import fg,bg

import fnlib_local
STANDALONE = False
try:
    from codeframe import config
    from codeframe import objects
except:
    STANDALONE = True
    print(f"i... {fg.yellow}STANDALONE MODE ... no objects, no config{fg.default}")
if __name__ == "__main__": STANDALONE = True
#KNOWN_COMMANDS_LOCAL_TYPE = "FS"
KNOWN_COMMANDS_LOCAL_TYPE = "OBJ"

# ================================================ ASC AND INI ==================
# taken from histo_io  refers to ascgeneric....
# I like it here, bc menu interaction
#





### def read_from_file(filename, fourcolumns = False, sort = True):



def time_cut(df, t_from, t_to):
    t0 = float(t_from)
    t1 = float(t_to)
    df1 = df.loc[ (df.time>t0)&(df.time<t1)]
    return df1

#


def s_load_asc( filename, channel  , existing_df = None, fourcolumns = False, sort = True,
                ini_time = None, fini_time = None,
                save = None
               ):  # complete filena or address
    """
    loads asc file, needs ini file too
    returns DF and inifile
    """
    # f = folder
    # if len(f)==0: f = "./"xxxx3
    # if f[-1]!="/": f=f+"/"

    #avadisc = list_asc_disc( folder = folder)
    # # present_avadisc(avadisc)

    inifilename = os.path.splitext(filename)[0]+".ini"
    ascfilename = os.path.splitext(filename)[0]+".asc"

    if filename.find("http://")==0:
        inifilename = get_ini_copy( inifilename )
        print(" ... local INI: ",inifilename)
    else:
        if not os.path.exists(ascfilename):
            print(f"X... {fg.red}no file {fg.default}", ascfilename)
            return existing_df, None


    if True:
        #####print(f"i... {bg.green} INI FILE PRESENT{bg.default}", inifilename)
        start_fname = filename_decode_starttime( filename )   #  - get start from filename TO REPORT
        start_exact = filename_decode_starttag( filename )
        discr =  abs(  (start_exact-start_fname).total_seconds()   )
        tag = f"{fg.green}"
        if discr>1.0:
            tag = f"{fg.red}"
        print(  "i...",tag, f" ... difference between FNAME start and TAG start is {discr} s.  {fg.default}")
        ####print("D... started", start, type(start) )
        #print(f"i...{bg.white}{fg.black} ... LOADING {ascfilename} ...                                                             {fg.default}{bg.default}")
        #wastart = dt.datetime.now()

        if existing_df is None:
            return
            #df = ascgeneric.pd_read_table( ascfilename, sort = True,  fourcolumns = False)
        df = existing_df
        if "time" not in existing_df.columns:
            #
            #  If already done, do not modify.....
            #  This is a final touch to make the DF nice
            #
            if len(df.columns)==5:
                df.columns = ['time','E','pu','ch','extras']
                df = df.dropna(axis='columns', how='all')
                df['time'] = df['time']/1e+8 # this is correct for 10ns step ADC
                # print("D... table last timestamp ",df.iloc[-1]["time"], "sec.")
                #print(df.dtypes)
                df['time'] = df['time'].astype('float64')
                df['E']    = df['E'].astype('int32')
                df['pu']   = df['pu'].astype('int32')
                df['ch']   = df['ch'].astype('int32')
                if 'extras' in df:
                    df['extras'] = df['extras'].astype('int32')
                #print("D...    finale types ************************************")
                #print(df.dtypes)

            if fourcolumns:
                print("D... extra operation for four columns ...  FOURCOLUMNS TABLE !!!")
                print(df)
                df['E'] = df['E']+ (df['pu']&1) * 16384
                df['pu'] = df['pu'].apply(lambda x: x >> 1)
                print(df)

            if sort:
                print(f"{fg.dimgray}D... sorting   (after read_table):{fg.default}")
                #print(df)
                df = df.sort_values(by="time")
                #print(df)
                df.reset_index(inplace=True, drop=True)

            else:
                print("X... {fg.red} ... not 5 columns - check the earlier versions to repair {fg.default}")
                return

        #
        # I HAVE ALL BASIC THINGS....  now :  CUTS in chanel !!!BUT STATISTICS!!!  ; time;  expand
        #
        # ***************************************************************************************
        chan_available = df['ch'].unique()
        #print(chan_available)
        if channel is None:
            print(f"X... {fg.red} give me channel number (e.g.  -c 1 ) {fg.default}")
            return df, start_exact
        channel = int(channel)

        if not channel in chan_available:
            print(f"X... channel {channel} {bg.red}     NOT PRESENT - in data    {bg.default}")
            return df, start_exact
        else:
            print(f"D... {fg.green} channel {channel} IS PRESENT - in data {fg.default}")

        #
        # SUCCESS
        #
        statistics(df )
        # SELECTION OF THE CHANNEL ********************
        df =  df.loc[ (df.ch==channel) ]
        print(f"i... {fg.limegreen} only channel #{channel} data remain! {fg.default}")
        if True:
            return df, start_exact # , inifilename, start_exact
        #
        #
        #
        # **********************************************************************************************
        # operations
        # **********************************************************************************************
        if ini_time is not None and fini_time is not None:
            print(f"{bg.yellow}{fg.black}=============== OPERATION=========================={bg.default}{fg.default}")
            if ini_time>df['time'].max(): return None
            if fini_time<df['time'].min(): return None
            df = time_cut(df, ini_time, fini_time)

        # **********************************************************************************************
        # operations
        # **********************************************************************************************
        TRGHOLD_us, PKHOLD_us, DBLZERO_us, ECALa, ECALb = load_ini_file( inifilename, channel ) # I never need channel here.. Later, after CUT

        if TRGHOLD_us is None  or  PKHOLD_us is None:
            print(f"x... {bg.red}NO INI FILE PRESENT{bg.default}")
            return existing_df, start_exact
        #print(df.head(1))
        #print(df.tail(1))
        #print(  df.iloc[[0,1, -2,-1]]  )
        # **********************************************************************************************

        #wastop = dt.datetime.now()-wastart
        #print(f"i... {bg.white}{fg.green} ... LOADED {len(df)/1000/1000:.2f} Mrows ... {len(df)/wastop.total_seconds()/1000/1000:.2f} Mrows/sec ", end="")
        #print( " "*40,f"{fg.default}{bg.default}")
        print()
        print(f"{fg.dimgray}D... enhancing DataFrame by dtus and dtus_prev {fg.default}", flush=True)
        df = enhance_by_dtus_and_next_E(df)
        # ********* STATISTICS ***********
        #stat_asc(df)
        # ***********STAT ON DEADTIME ***********
        DTP = asc_stat_print(df, TIMEWIN_US=PKHOLD_us,  filename = ascfilename, TRGHOLD = TRGHOLD_us , DZEROES_US = DBLZERO_us )



# =====================================================================================================
# =====================================================================================================
# --------------------------    MAIN ------------------------------------------------------------------
# =====================================================================================================
# =====================================================================================================

def require_parameter(parname, args, kwargs):
    if parname in kwargs:
        name = kwargs[parname]
        print(f"D... parname: {parname} .... type: {type(name)} ... {name}")
        return name
    elif parname in args:
        return True
    else:
        return None




# =====================================================================================================
# =====================================================================================================

def main(*args,c):
    """
    select channel AND MAINLY PROCESS ASC FILE AS RUN EVENTBYENEVT  - on the dataframe object
    --channel or -c
    """
    channel = int(c) # MUST BE CONVERTED TO INTEGER !!!!
    print(f"{fg.dimgray}D... main() @fn_selchan: args ### /{args}/ ... channel:{c}{fg.default}")
    if channel is None:
        print(f"X... {fg.red} give the channel number (like -c 0 OR  -c 1) {fg.default}")

    oname = ""
    if len(args)==0:
        print(f"D...  {fg.red}argument needed - type: OBJECT {fg.default}" )
        oname = "run0132_221002_140349_1Mo31p4.asc"
        channel = 1
        #return None
    else:
        oname = args[0]


    # **************************************************************will not work...
    if STANDALONE:
        print(f"D... {fg.dimgray} running standalone mode{fg.default}")
        #df = require_parameter("existing_df", kwargs )
        #channel = channel require_parameter("channel", args)
        if channel is None:
            print(f"X... {fg.red} give the channel number ( --channel 1) {fg.default}")
        it = require_parameter("ini_time", args ,kwargs)
        ft = require_parameter("fini_time", args , kwargs)
        if it is None or ft is None:
            print("D... possible options for CUT:  --ini_time 60 --fini_time 120")
        save = require_parameter("save", args , kwargs)
        if save is None:
            print("D... possible options for save:  --save ")
        #
        # Using import from another local library
        df = fnlib_local.read_from_file(oname)
        # filename and existing df
        newdf,start_exact = fnlib_local.get_start_info( oname, channel, df, ini_time = it, fini_time = ft, save = save)
        #  I AM DONE - NOW I work with an object ----
        print(  newdf.iloc[[0,1, -2,-1]]  )
        print("-"*60)

        return
    # **************************************************************
    # print( oname,"?", list( objects.get_objects_list() ) )

    if objects.object_exists(oname):
        obj = objects.get_object( oname )        #print(f"i... exists  {fg.green}{oname}{fg.default} == {obj} ")
        if obj is None:
            print(f"i... {fg.red} cant get  {oname} OBJECT{fg.default}")
            return False
        if type(obj)==objects.O_dataframe:
            print(f"i... showing {fg.green}{oname}{fg.default}  ")        #objects.list_objects(  )
            #channel = require_parameter("channel", args )
            # if channel is None:
            #     print(f"X... {fg.red} give me channel number (e.g.  -c 1 ) {fg.default}")
            #     return
            #
            #print(obj.src_filename)
            #print(channel)
            #print(obj.df_data)
            start_exact = fnlib_local.get_start_info( obj.src_filename, channel, obj.df_data)
            obj.df_data = fnlib_local.label_sort( obj.df_data )
            fnlib_local.statistics( obj.df_data, obj.src_filename )

            print(f"i... {fg.yellow} reducing to channel {channel} !{fg.default}", end = " ")
            #print( obj.df_data)
            obj.df_data =  obj.df_data.loc[ (obj.df_data.ch==channel) ]
            obj.df_data.reset_index(inplace=True, drop=True)
            obj.comment = f"channel {channel} only; "
            DLEFT=len(obj.df_data)
            if DLEFT==0:
                print(0)
                print(f"X... {fg.red} NO DATA LEFT after reducing to channel {channel} !!! {fg.default}" )
            else:
                print(DLEFT)
            #print( obj.df_data)

            obj.elements = len(obj.df_data)
            obj.started = start_exact
            #print(started)
            #print( "D... type==",type(obj) ," .. ", type(obj)==objects.O_dataframe )
            #print( obj.__dict__ )
            #obj.print()
            #print(obj.get_name() )
            #print(obj.about() )
            #print(obj.abouts() )
            # ------------------------  object contains  printme function: -----------
            # if "printme" in dir(obj):    #print("D...    printme is there...")
            #     print(obj.printme() )
            # else:
            #     return False
            #print(f"i... obj function called  {fg.green}{oname}{fg.default}  ")
        else:
            print("X... not type O_dataframe")
    #**************************************************************************
    else:
        print(f"i... {fg.red} NOT showing {oname}{fg.default}")
    return True



if __name__=="__main__":
    #Fire(read_from_file)
    Fire(main)
    # run0142_221002_232850_1mo37p2.asc
